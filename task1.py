class DimensionsOutOfBoundError(Exception):
    def __init__(self, text):
        self.text = text


class Package:
    def __init__(self, length, width, height, weight=0, volume=0):
        self.length = length
        self.width = width
        self.height = height
        self.weight = weight
        self.volume = volume

    @property
    def length(self):
        return self.l

    @length.setter
    def length(self, value):
        self.l = value
        if 0 >= value or value > 350:
            raise DimensionsOutOfBoundError(f'Package length=={value} out of bounds, should be: 0 < length <= 350')

    @property
    def width(self):
        return self.w

    @width.setter
    def width(self, value):
        self.w = value
        if 0 >= value or value > 300:
            raise DimensionsOutOfBoundError(f'Package width=={value} out of bounds, should be: 0 < width <= 300')

    @property
    def height(self):
        return self.h

    @height.setter
    def height(self, value):
        self.h = value
        if 0 >= value or value > 150:
            raise DimensionsOutOfBoundError(f'Package height=={value} out of bounds, should be: 0 < height <= 150')

    @property
    def weight(self):
        return self.we

    @weight.setter
    def weight(self, value):
        self.we = value
        if 0 > value or value > 40:
            raise DimensionsOutOfBoundError(f'Package weight=={value} out of bounds, should be: 0 < weight <= 40')

    @property
    def volume(self):
        return self.v

    @volume.setter
    def volume(self, value):
        value = self.length * self.width * self.height
        self.v = value
