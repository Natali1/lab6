import json


def jsonattr(text):
    with open(text, 'r', encoding='utf8') as fh:
        data = json.load(fh)

    def wrapper(func):
        if data:
            for f_key, f_val in data.items():
                setattr(func, f_key, f_val)
            return func
        else:
            return func
    return wrapper


